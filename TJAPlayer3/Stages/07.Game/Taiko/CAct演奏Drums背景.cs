﻿using System;
using System.Linq.Expressions;
using FDK;

namespace TJAPlayer3
{
    internal class CAct演奏Drums背景 : CActivity
    {
        // 本家っぽい背景を表示させるメソッド。
        //
        // 拡張性とかないんで。はい、ヨロシクゥ!
        //
        public CAct演奏Drums背景()
        {
            base.b活性化してない = true;
        }

        public void ClearIn(int player)
        {
            this.ct上背景クリアインタイマー[player] = new CCounter(0, 100, 2, TJAPlayer3.Timer);
            this.ct上背景クリアインタイマー[player].n現在の値 = 0;
            this.ct上背景FIFOタイマー = new CCounter(0, 100, 2, TJAPlayer3.Timer);
            this.ct上背景FIFOタイマー.n現在の値 = 0;
        }

        public override void On非活性化()
        {
            ct上背景FIFOタイマー = null;

            for (int i = 0; i < 2; i++)
            {
                ct上背景スクロール用タイマー[i] = null;
            }

            ct下背景スクロール用タイマー1 = null;

            base.On非活性化();
        }

        public override void OnManagedリソースの作成()
        {
            //this.tx上背景メイン = CDTXMania.tテクスチャの生成( CSkin.Path( @"Graphics\Upper_BG\01\bg.png" ) );
            //this.tx上背景クリアメイン = CDTXMania.tテクスチャの生成( CSkin.Path( @"Graphics\Upper_BG\01\bg_clear.png" ) );
            //this.tx下背景メイン = CDTXMania.tテクスチャの生成( CSkin.Path( @"Graphics\Dancer_BG\01\bg.png" ) );
            //this.tx下背景クリアメイン = CDTXMania.tテクスチャの生成( CSkin.Path( @"Graphics\Dancer_BG\01\bg_clear.png" ) );
            //this.tx下背景クリアサブ1 = CDTXMania.tテクスチャの生成( CSkin.Path( @"Graphics\Dancer_BG\01\bg_clear_01.png" ) );
            //this.ct上背景スクロール用タイマー = new CCounter( 1, 328, 40, CDTXMania.Timer );
            this.ct上背景スクロール用タイマー = new CCounter[2];
            this.ct上背景スクロール用タイマーD = new CCounter[2];
            this.ct上背景スクロール用タイマーD1 = new CCounter[2];
            this.ct上背景スクロール用タイマーDW = new CCounter[2];
            this.ct上背景スクロール用タイマーD2 = new CCounter[2];
            this.ct上背景スクロール用タイマーD3 = new CCounter[2];
            this.ct上背景スクロール用タイマーD4 = new CCounter[2];
            this.ct上背景上スクロール用タイマー = new CCounter[2];
            this.ct上背景クリアインタイマー = new CCounter[2];
            this.ct上背景スクロール用タイマーU = new CCounter[2];
            this.ct上背景スクロール用タイマーU1 = new CCounter[2];
            this.ct上背景スクロール用タイマーUd = new CCounter[2];
            this.ct上背景スクロール用タイマーUd1 = new CCounter[2];
            for (int i = 0; i < 2; i++)
            {
                if (TJAPlayer3.Tx.Background_Up[i] != null)
                {
                    this.ct上背景スクロール用タイマー[i] = new CCounter(1, TJAPlayer3.Tx.Background_Up[i].szテクスチャサイズ.Width, 16, TJAPlayer3.Timer);
　　　　　　　　　  this.ct上背景上スクロール用タイマー[i] = new CCounter(0, 154, 62, TJAPlayer3.Timer);
                    this.ct上背景クリアインタイマー[i] = new CCounter();
                }
                if (TJAPlayer3.Tx.Background_Up3[i] != null)
                {
                    this.ct上背景スクロール用タイマーU[i] = new CCounter(0, TJAPlayer3.Tx.Background_Up3[i].szテクスチャサイズ.Width, 16, TJAPlayer3.Timer);
                    this.ct上背景スクロール用タイマーU1[i] = new CCounter(0, TJAPlayer3.Tx.Background_Up3[i].szテクスチャサイズ.Width, 4, TJAPlayer3.Timer);
                    this.ct上背景スクロール用タイマーUd[i] = new CCounter(0, TJAPlayer3.Tx.Background_Up3[i].szテクスチャサイズ.Width, 16, TJAPlayer3.Timer);
                    this.ct上背景スクロール用タイマーUd1[i] = new CCounter(0, TJAPlayer3.Tx.Background_Up3[i].szテクスチャサイズ.Width, 8, TJAPlayer3.Timer);//この子あまり関係なくなってきたが一応残しておく大ジャンプ時のスクロールを早くさせたくて置いたもの。
                    this.ct上背景クリアインタイマー[i] = new CCounter();
                }
                #region[DAN]
                if (TJAPlayer3.Tx.Background_UpDanC1[i] != null)
                {
                    this.ct上背景スクロール用タイマーD[i] = new CCounter(1, TJAPlayer3.Tx.Background_UpDanC1[i].szテクスチャサイズ.Width, 20, TJAPlayer3.Timer);
                    this.ct上背景スクロール用タイマーD1[i] = new CCounter(1, TJAPlayer3.Tx.Background_UpDanC2[i].szテクスチャサイズ.Width, 17, TJAPlayer3.Timer);
                    this.ct上背景スクロール用タイマーD2[i] = new CCounter(1, TJAPlayer3.Tx.Background_UpDanC3[i].szテクスチャサイズ.Width, 32, TJAPlayer3.Timer);
                    this.ct上背景スクロール用タイマーDW[i] = new CCounter(1, TJAPlayer3.Tx.Background_UpDanC4[i].szテクスチャサイズ.Width, 10, TJAPlayer3.Timer);
                    this.ct上背景スクロール用タイマーD3[i] = new CCounter(1, TJAPlayer3.Tx.Background_UpDanC5[i].szテクスチャサイズ.Width, 19, TJAPlayer3.Timer);
                    this.ct上背景スクロール用タイマーD[i] = new CCounter(1, 1280, 20, TJAPlayer3.Timer);
                    this.ct上背景スクロール用タイマーD1[i] = new CCounter(1, 1333, 17, TJAPlayer3.Timer);
                    this.ct上背景スクロール用タイマーD2[i] = new CCounter(1, 1280, 32, TJAPlayer3.Timer);
                    this.ct上背景スクロール用タイマーDW[i] = new CCounter(1, 1280, 10, TJAPlayer3.Timer);
                    this.ct上背景スクロール用タイマーD3[i] = new CCounter(1, 1266, 19, TJAPlayer3.Timer);
                    this.ct上背景スクロール用タイマーD4[i] = new CCounter(1, 600, 12, TJAPlayer3.Timer);
                }
                #endregion

            }

            if (TJAPlayer3.Tx.Background_Down_Scroll != null)
                this.ct下背景スクロール用タイマー1 = new CCounter( 1, TJAPlayer3.Tx.Background_Down_Scroll.szテクスチャサイズ.Width, 30, TJAPlayer3.Timer );
                this.ct下背景スクロール用タイマー1 = new CCounter(1, 1280, 30, TJAPlayer3.Timer);
            if (TJAPlayer3.Tx.Background_Down_Scroll1 != null)
                this.ct下背景スクロール用タイマー2 = new CCounter(1, TJAPlayer3.Tx.Background_Down_Scroll1.szテクスチャサイズ.Width, 14, TJAPlayer3.Timer);
                this.ct下背景スクロール用タイマー2 = new CCounter(1, 1280, 14, TJAPlayer3.Timer);
            if (TJAPlayer3.Tx.Background_Down_Scroll2 != null)
                this.ct下背景スクロール用タイマー3 = new CCounter(1, TJAPlayer3.Tx.Background_Down_Scroll1.szテクスチャサイズ.Width, 18, TJAPlayer3.Timer);
                this.ct下背景スクロール用タイマー3 = new CCounter(1, 1280, 18, TJAPlayer3.Timer);
            if (TJAPlayer3.Tx.Background_Down_UD != null)
                this.ct下背景上下用 = new CCounter(0, 60, 35, TJAPlayer3.Timer);
            this.cttoumeib = new CCounter(0, 14, 24, TJAPlayer3.Timer);
            this.ct上背景FIFOタイマー = new CCounter();
            base.OnManagedリソースの作成();
        }

        public override int On進行描画()
        {
            this.ct上背景FIFOタイマー.t進行();
            
            for (int i = 0; i < 2; i++)
            {
                if(this.ct上背景クリアインタイマー[i] != null)
                   this.ct上背景クリアインタイマー[i].t進行();
            }
            this.cttoumeib.t進行Loop();
            int c = 0;

            if (this.cttoumeib.n現在の値 <= 7)
            {
                c = 56 + cttoumeib.n現在の値;
            }
            else
            {
                c = 70 - cttoumeib.n現在の値;
            }
            #region[上背景]
            for (int i = 0; i < 2; i++)
            {
                if (this.ct上背景スクロール用タイマー[i] != null)
                    this.ct上背景スクロール用タイマー[i].t進行Loop();
                if (this.ct上背景スクロール用タイマーD[i] != null)
                    this.ct上背景スクロール用タイマーD[i].t進行Loop();
                if (this.ct上背景スクロール用タイマーD1[i] != null)
                    this.ct上背景スクロール用タイマーD1[i].t進行Loop();
                if (this.ct上背景スクロール用タイマーD2[i] != null)
                    this.ct上背景スクロール用タイマーD2[i].t進行Loop();
                if (this.ct上背景スクロール用タイマーDW[i] != null)
                    this.ct上背景スクロール用タイマーDW[i].t進行Loop();
                if (this.ct上背景スクロール用タイマーD3[i] != null)
                    this.ct上背景スクロール用タイマーD3[i].t進行Loop();
                if (this.ct上背景スクロール用タイマーD4[i] != null)
                    this.ct上背景スクロール用タイマーD4[i].t進行Loop();
                if (this.ct上背景上スクロール用タイマー[i] != null)
                    this.ct上背景上スクロール用タイマー[i].t進行Loop();
                if (this.ct上背景スクロール用タイマーU[i] != null)
                    this.ct上背景スクロール用タイマーU[i].t進行Loop();
                if (this.ct上背景スクロール用タイマーU1[i] != null)
                    this.ct上背景スクロール用タイマーU1[i].t進行Loop();
                if (this.ct上背景スクロール用タイマーUd[i] != null)
                    this.ct上背景スクロール用タイマーUd[i].t進行Loop();
                if (this.ct上背景スクロール用タイマーUd1[i] != null)
                    this.ct上背景スクロール用タイマーUd1[i].t進行Loop();
            }
            #endregion
            #region [下背景]
            if (this.ct下背景スクロール用タイマー1 != null)
                this.ct下背景スクロール用タイマー1.t進行Loop();
            if (this.ct下背景スクロール用タイマー2 != null)
                this.ct下背景スクロール用タイマー2.t進行Loop();
            if (this.ct下背景スクロール用タイマー3 != null)
                this.ct下背景スクロール用タイマー3.t進行Loop();
            if (this.ct下背景上下用 != null)
                this.ct下背景上下用.t進行Loop();
            #endregion
            #region 1P-2P-上背景Normal
            if (TJAPlayer3.stage選曲.n確定された曲の難易度 != (int)Difficulty.Dan)
                            {
                                if (TJAPlayer3.ConfigIni.PlayNo == false)
                                    for (int i = 0; i < 2; i++)
                                    {

                                        if (this.ct上背景スクロール用タイマー[i] != null)
                                        {
                                            double TexSize = 1280 / TJAPlayer3.Tx.Background_Up[i].szテクスチャサイズ.Width;
                                            int ForLoop = (int)Math.Ceiling(TexSize) + 1;
                                            TJAPlayer3.Tx.Background_Up2[i].t2D描画(TJAPlayer3.app.Device, 0 - this.ct上背景スクロール用タイマー[i].n現在の値, TJAPlayer3.Skin.Background_Scroll_Y[i] - this.ct上背景上スクロール用タイマー[i].n現在の値);
                                            TJAPlayer3.Tx.Background_Up[i].t2D描画(TJAPlayer3.app.Device, 0 - this.ct上背景スクロール用タイマー[i].n現在の値, TJAPlayer3.Skin.Background_Scroll_Y[i] - 154 + this.ct上背景上スクロール用タイマー[i].n現在の値);
                                            for (int a = 0; a < 3; a++)
                                            {
                                                for (int l = 1; l < ForLoop + 1; l++)
                                                {
                                                    TJAPlayer3.Tx.Background_Up2[i].t2D描画(TJAPlayer3.app.Device, +(l * TJAPlayer3.Tx.Background_Up[i].szテクスチャサイズ.Width) - this.ct上背景スクロール用タイマー[i].n現在の値, TJAPlayer3.Skin.Background_Scroll_Y[i] - this.ct上背景上スクロール用タイマー[i].n現在の値);
                                                    TJAPlayer3.Tx.Background_Up[i].t2D描画(TJAPlayer3.app.Device, +(l * TJAPlayer3.Tx.Background_Up[i].szテクスチャサイズ.Width) - this.ct上背景スクロール用タイマー[i].n現在の値, TJAPlayer3.Skin.Background_Scroll_Y[i] - 154 + this.ct上背景上スクロール用タイマー[i].n現在の値);
                                                }
                                            }

                                        }
                                        if (this.ct上背景スクロール用タイマーU[i] != null && this.ct上背景スクロール用タイマーUd[i] != null && this.ct上背景スクロール用タイマーU1[i] != null && this.ct上背景スクロール用タイマーUd1[i] != null)
                                        {
                                            double 回転値 = 180.0;
                                            double b = Math.Sin(this.ct上背景スクロール用タイマーU1[i].n現在の値 * Math.PI / (this.ct上背景スクロール用タイマーU1[i].n終了値 * 180.0 / 回転値)) * 23;//小ジャンプ時の三角関数文
                                            double d = Math.Sin(this.ct上背景スクロール用タイマーUd1[i].n現在の値 * Math.PI / (this.ct上背景スクロール用タイマーUd1[i].n終了値 * 180.0 / 回転値)) * 30;//大ジャンプ時の三角関数文 //.クリアも同様


                                            double TexSize1 = 1280 / TJAPlayer3.Tx.Background_Up3[i].szテクスチャサイズ.Width;
                                            int ForLoop = (int)Math.Ceiling(TexSize1) + 1;
                                            TJAPlayer3.Tx.Background_Up3[i].t2D描画(TJAPlayer3.app.Device, 0 - (this.ct上背景スクロール用タイマーU[i].n現在の値), (float)(TJAPlayer3.Skin.Background_Scroll_Y[i] - 42 + b + d));

                                            for (int l = 1; l < ForLoop + 1; l++)
                                            {
                                                TJAPlayer3.Tx.Background_Up3[i].t2D描画(TJAPlayer3.app.Device, 0 + (l * TJAPlayer3.Tx.Background_Up[i].szテクスチャサイズ.Width) - (this.ct上背景スクロール用タイマーU[i].n現在の値), (float)(TJAPlayer3.Skin.Background_Scroll_Y[i] - 42 + b + d));
                                            }

                                        }


                                    }

                                else if (TJAPlayer3.stage演奏ドラム画面.bDoublePlay == false)
                                {
                                    for (int i = 0; i < 2; i++)
                                    {
                                        if (this.ct上背景スクロール用タイマー[i] != null)
                                        {
                                            double TexSize = 1280 / TJAPlayer3.Tx.Background_Up[i].szテクスチャサイズ.Width;
                                            int ForLoop = (int)Math.Ceiling(TexSize) + 1;
                                            TJAPlayer3.Tx.Background_Up2[i].t2D描画(TJAPlayer3.app.Device, 0 - this.ct上背景スクロール用タイマー[i].n現在の値, TJAPlayer3.Skin.Background_Scroll_Y[i] - 540 - this.ct上背景上スクロール用タイマー[i].n現在の値);
                                            TJAPlayer3.Tx.Background_Up[i].t2D描画(TJAPlayer3.app.Device, 0 - this.ct上背景スクロール用タイマー[i].n現在の値, TJAPlayer3.Skin.Background_Scroll_Y[i] - 694 + this.ct上背景上スクロール用タイマー[i].n現在の値);
                                            for (int a = 0; a < 3; a++)
                                            {
                                                for (int l = 1; l < ForLoop + 1; l++)
                                                {
                                                    TJAPlayer3.Tx.Background_Up2[i].t2D描画(TJAPlayer3.app.Device, +(l * TJAPlayer3.Tx.Background_Up[i].szテクスチャサイズ.Width) - this.ct上背景スクロール用タイマー[i].n現在の値, TJAPlayer3.Skin.Background_Scroll_Y[i] - 540 - this.ct上背景上スクロール用タイマー[i].n現在の値);
                                                    TJAPlayer3.Tx.Background_Up[i].t2D描画(TJAPlayer3.app.Device, +(l * TJAPlayer3.Tx.Background_Up[i].szテクスチャサイズ.Width) - this.ct上背景スクロール用タイマー[i].n現在の値, TJAPlayer3.Skin.Background_Scroll_Y[i] - 694 + this.ct上背景上スクロール用タイマー[i].n現在の値);
                                                }
                                            }

                                        }
                                        if (this.ct上背景スクロール用タイマーU[i] != null && this.ct上背景スクロール用タイマーUd[i] != null && this.ct上背景スクロール用タイマーU1[i] != null && this.ct上背景スクロール用タイマーUd1[i] != null)
                                        {
                                            double 回転値 = 180.0;
                                            double b = Math.Sin(this.ct上背景スクロール用タイマーU1[i].n現在の値 * Math.PI / (this.ct上背景スクロール用タイマーU1[i].n終了値 * 180.0 / 回転値)) * 23;//小ジャンプ時の三角関数文
                                            double d = Math.Sin(this.ct上背景スクロール用タイマーUd1[i].n現在の値 * Math.PI / (this.ct上背景スクロール用タイマーUd1[i].n終了値 * 180.0 / 回転値)) * 30;//大ジャンプ時の三角関数文 //.クリアも同様


                                            double TexSize1 = 1280 / TJAPlayer3.Tx.Background_Up3[i].szテクスチャサイズ.Width;
                                            int ForLoop = (int)Math.Ceiling(TexSize1) + 1;
                                            TJAPlayer3.Tx.Background_Up3[i].t2D描画(TJAPlayer3.app.Device, 0 - (this.ct上背景スクロール用タイマーU[i].n現在の値), (float)(TJAPlayer3.Skin.Background_Scroll_Y[i] - 535 - 42 + b + d));

                                            for (int l = 1; l < ForLoop + 1; l++)
                                            {
                                                TJAPlayer3.Tx.Background_Up3[i].t2D描画(TJAPlayer3.app.Device, 0 + (l * TJAPlayer3.Tx.Background_Up[i].szテクスチャサイズ.Width) - (this.ct上背景スクロール用タイマーU[i].n現在の値), (float)(TJAPlayer3.Skin.Background_Scroll_Y[i] - 535 - 42 + b + d));
                                            }

                                        }

                                    }
                                }
                                if (TJAPlayer3.stage演奏ドラム画面.bDoublePlay == true)
                                    for (int i = 0; i < 2; i++)
                                    {

                                        if (this.ct上背景スクロール用タイマー[i] != null)
                                        {
                                            double TexSize = 1280 / TJAPlayer3.Tx.Background_Up[i].szテクスチャサイズ.Width;
                                            int ForLoop = (int)Math.Ceiling(TexSize) + 1;
                                            TJAPlayer3.Tx.Background_Up2[i].t2D描画(TJAPlayer3.app.Device, 0 - this.ct上背景スクロール用タイマー[i].n現在の値, TJAPlayer3.Skin.Background_Scroll_Y[i] - this.ct上背景上スクロール用タイマー[i].n現在の値);
                                            TJAPlayer3.Tx.Background_Up[i].t2D描画(TJAPlayer3.app.Device, 0 - this.ct上背景スクロール用タイマー[i].n現在の値, TJAPlayer3.Skin.Background_Scroll_Y[i] - 154 + this.ct上背景上スクロール用タイマー[i].n現在の値);
                                            for (int a = 0; a < 3; a++)
                                            {
                                                for (int l = 1; l < ForLoop + 1; l++)
                                                {
                                                    TJAPlayer3.Tx.Background_Up2[i].t2D描画(TJAPlayer3.app.Device, +(l * TJAPlayer3.Tx.Background_Up[i].szテクスチャサイズ.Width) - this.ct上背景スクロール用タイマー[i].n現在の値, TJAPlayer3.Skin.Background_Scroll_Y[i] - this.ct上背景上スクロール用タイマー[i].n現在の値);
                                                    TJAPlayer3.Tx.Background_Up[i].t2D描画(TJAPlayer3.app.Device, +(l * TJAPlayer3.Tx.Background_Up[i].szテクスチャサイズ.Width) - this.ct上背景スクロール用タイマー[i].n現在の値, TJAPlayer3.Skin.Background_Scroll_Y[i] - 154 + this.ct上背景上スクロール用タイマー[i].n現在の値);
                                                }
                                            }

                                        }
                                        if (this.ct上背景スクロール用タイマーU[i] != null && this.ct上背景スクロール用タイマーUd[i] != null && this.ct上背景スクロール用タイマーU1[i] != null && this.ct上背景スクロール用タイマーUd1[i] != null)
                                        {
                                            double 回転値 = 180.0;
                                            double b = Math.Sin(this.ct上背景スクロール用タイマーU1[i].n現在の値 * Math.PI / (this.ct上背景スクロール用タイマーU1[i].n終了値 * 180.0 / 回転値)) * 23;//小ジャンプ時の三角関数文
                                            double d = Math.Sin(this.ct上背景スクロール用タイマーUd1[i].n現在の値 * Math.PI / (this.ct上背景スクロール用タイマーUd1[i].n終了値 * 180.0 / 回転値)) * 30;//大ジャンプ時の三角関数文 //.クリアも同様


                                            double TexSize1 = 1280 / TJAPlayer3.Tx.Background_Up3[i].szテクスチャサイズ.Width;
                                            int ForLoop = (int)Math.Ceiling(TexSize1) + 1;
                                            TJAPlayer3.Tx.Background_Up3[i].t2D描画(TJAPlayer3.app.Device, 0 - (this.ct上背景スクロール用タイマーU[i].n現在の値), (float)(TJAPlayer3.Skin.Background_Scroll_Y[i] - 42 + b + d));

                                            for (int l = 1; l < ForLoop + 1; l++)
                                            {
                                                TJAPlayer3.Tx.Background_Up3[i].t2D描画(TJAPlayer3.app.Device, 0 + (l * TJAPlayer3.Tx.Background_Up[i].szテクスチャサイズ.Width) - (this.ct上背景スクロール用タイマーU[i].n現在の値), (float)(TJAPlayer3.Skin.Background_Scroll_Y[i] - 42 + b + d));
                                            }

                                        }


                                    }
                                for (int i = 0; i < 2; i++)
                                    if (this.ct上背景スクロール用タイマー[i] != null)
                                    {
                                        if (TJAPlayer3.stage演奏ドラム画面.bIsAlreadyCleared[i] && TJAPlayer3.ConfigIni.eGaugeMode != EGaugeMode.Hard && TJAPlayer3.ConfigIni.eGaugeMode != EGaugeMode.ExHard)
                                            TJAPlayer3.Tx.Background_UpC[i].Opacity = ((this.ct上背景クリアインタイマー[i].n現在の値 * 0xff) / 100);
                                        else
                                            TJAPlayer3.Tx.Background_UpC[i].Opacity = 0;

                                        if (TJAPlayer3.stage演奏ドラム画面.bIsAlreadyCleared[i] && TJAPlayer3.ConfigIni.eGaugeMode != EGaugeMode.Hard && TJAPlayer3.ConfigIni.eGaugeMode != EGaugeMode.ExHard)
                                            TJAPlayer3.Tx.Background_Up2C[i].Opacity = ((this.ct上背景クリアインタイマー[i].n現在の値 * 0xff) / 100);
                                        else
                                            TJAPlayer3.Tx.Background_Up2C[i].Opacity = 0;

                                        if (TJAPlayer3.stage演奏ドラム画面.bIsAlreadyCleared[i] && TJAPlayer3.ConfigIni.eGaugeMode != EGaugeMode.Hard && TJAPlayer3.ConfigIni.eGaugeMode != EGaugeMode.ExHard)
                                            TJAPlayer3.Tx.Background_Up3C[i].Opacity = ((this.ct上背景クリアインタイマー[i].n現在の値 * 0xff) / 100);
                                        else
                                            TJAPlayer3.Tx.Background_Up3C[i].Opacity = 0;



                                        double TexSize = 1280 / TJAPlayer3.Tx.Background_Up[i].szテクスチャサイズ.Width;
                                        // 1280をテクスチャサイズで割ったものを切り上げて、プラス+1足す。
                                        int ForLoop = (int)Math.Ceiling(TexSize) + 1;
                                        //int nループ幅 = 328;
                                        TJAPlayer3.Tx.Background_Up2C[i].t2D描画(TJAPlayer3.app.Device, 0 - this.ct上背景スクロール用タイマー[i].n現在の値, TJAPlayer3.Skin.Background_Scroll_Y[i] - this.ct上背景上スクロール用タイマー[i].n現在の値);
                                        TJAPlayer3.Tx.Background_UpC[i].t2D描画(TJAPlayer3.app.Device, 0 - this.ct上背景スクロール用タイマー[i].n現在の値, TJAPlayer3.Skin.Background_Scroll_Y[i] - 154 + this.ct上背景上スクロール用タイマー[i].n現在の値);

                                        for (int a = 0; a < 3; a++)
                                        {
                                            for (int l = 1; l < ForLoop + 1; l++)
                                            {
                                                TJAPlayer3.Tx.Background_Up2C[i].t2D描画(TJAPlayer3.app.Device, +(l * TJAPlayer3.Tx.Background_Up[i].szテクスチャサイズ.Width) - this.ct上背景スクロール用タイマー[i].n現在の値, TJAPlayer3.Skin.Background_Scroll_Y[i] - this.ct上背景上スクロール用タイマー[i].n現在の値);

                                                TJAPlayer3.Tx.Background_UpC[i].t2D描画(TJAPlayer3.app.Device, +(l * TJAPlayer3.Tx.Background_Up[i].szテクスチャサイズ.Width) - this.ct上背景スクロール用タイマー[i].n現在の値, TJAPlayer3.Skin.Background_Scroll_Y[i] - 154 + this.ct上背景上スクロール用タイマー[i].n現在の値);
                                            }
                                        }
                                        double 回転値 = 180.0;
                                        double b = Math.Sin(this.ct上背景スクロール用タイマーU1[i].n現在の値 * Math.PI / (this.ct上背景スクロール用タイマーU1[i].n終了値 * 180.0 / 回転値)) * 23;
                                        double d = Math.Sin(this.ct上背景スクロール用タイマーUd1[i].n現在の値 * Math.PI / (this.ct上背景スクロール用タイマーUd1[i].n終了値 * 180.0 / 回転値)) * 30;
                                        double TexSize1 = 1280 / TJAPlayer3.Tx.Background_Up3C[i].szテクスチャサイズ.Width;
                                        int ForLoop1 = (int)Math.Ceiling(TexSize1) + 1;

                                        TJAPlayer3.Tx.Background_Up3C[i].t2D描画(TJAPlayer3.app.Device, 0 - this.ct上背景スクロール用タイマーU[i].n現在の値, (float)(TJAPlayer3.Skin.Background_Scroll_Y[i] - 42 + b + d));

                                        for (int l = 1; l < ForLoop1 + 1; l++)
                                        {
                                            TJAPlayer3.Tx.Background_Up3C[i].t2D描画(TJAPlayer3.app.Device, 0 + (l * TJAPlayer3.Tx.Background_Up[i].szテクスチャサイズ.Width) - this.ct上背景スクロール用タイマーU[i].n現在の値, (float)(TJAPlayer3.Skin.Background_Scroll_Y[i] - 42 + b + d));
                                        }
                                    }
                            }
            #region [DAN]
            if (TJAPlayer3.stage選曲.n確定された曲の難易度 != (int)Difficulty.Oni)
            if (TJAPlayer3.stage選曲.n確定された曲の難易度 != (int)Difficulty.Normal)
            if (TJAPlayer3.stage選曲.n確定された曲の難易度 != (int)Difficulty.Hard)
            if (TJAPlayer3.stage選曲.n確定された曲の難易度 != (int)Difficulty.Edit)
            if (TJAPlayer3.stage選曲.n確定された曲の難易度 != (int)Difficulty.Easy)
            {
                for (int i = 0; i < 2; i++)
                {
                    if (this.ct上背景スクロール用タイマー[i] != null)
                    {
                            TJAPlayer3.Tx.Background_UpDanC.t2D描画(TJAPlayer3.app.Device, 0, TJAPlayer3.Skin.Background_Scroll_Y[i]); 
                    }
                                    if (this.ct上背景スクロール用タイマーD[i] != null && this.ct上背景スクロール用タイマーD1[i] != null)
                                    {
                                        double TexSize = 1280 / TJAPlayer3.Tx.Background_UpDanC1[i].szテクスチャサイズ.Width;
                                        int ForLoop = (int)Math.Ceiling(TexSize) + 1;
                                        for (int a = 0; a < 3; a++)
                                        {
                                            TJAPlayer3.Tx.Background_UpDanC1[i].t2D描画(TJAPlayer3.app.Device, -this.ct上背景スクロール用タイマーD1[i].n現在の値, TJAPlayer3.Skin.Background_Scroll_Y[i]);
                                            TJAPlayer3.Tx.Background_UpDanC2[i].t2D描画(TJAPlayer3.app.Device, -this.ct上背景スクロール用タイマーD[i].n現在の値, TJAPlayer3.Skin.Background_Scroll_Y[i]);
                                            TJAPlayer3.Tx.Background_UpDanC5[i].t2D描画(TJAPlayer3.app.Device, -this.ct上背景スクロール用タイマーD3[i].n現在の値, TJAPlayer3.Skin.Background_Scroll_Y[i]);
                                            TJAPlayer3.Tx.Background_UpDanC4[i].t2D描画(TJAPlayer3.app.Device,  -this.ct上背景スクロール用タイマーDW[i].n現在の値, TJAPlayer3.Skin.Background_Scroll_Y[i] - 300 + this.ct上背景スクロール用タイマーD4[i].n現在の値);
                                            TJAPlayer3.Tx.Background_UpDanC3[i].t2D描画(TJAPlayer3.app.Device, -this.ct上背景スクロール用タイマーD2[i].n現在の値, TJAPlayer3.Skin.Background_Scroll_Y[i]);
                                            for (int l = 1; l < ForLoop + 1; l++)
                                            {
                                                TJAPlayer3.Tx.Background_UpDanC1[i].t2D描画(TJAPlayer3.app.Device, +(l * TJAPlayer3.Tx.Background_UpDanC1[i].szテクスチャサイズ.Width) - this.ct上背景スクロール用タイマーD1[i].n現在の値, TJAPlayer3.Skin.Background_Scroll_Y[i]);
                                                TJAPlayer3.Tx.Background_UpDanC2[i].t2D描画(TJAPlayer3.app.Device, +(l * TJAPlayer3.Tx.Background_UpDanC2[i].szテクスチャサイズ.Width) - this.ct上背景スクロール用タイマーD[i].n現在の値, TJAPlayer3.Skin.Background_Scroll_Y[i]);
                                                TJAPlayer3.Tx.Background_UpDanC5[i].t2D描画(TJAPlayer3.app.Device, +(l * TJAPlayer3.Tx.Background_UpDanC5[i].szテクスチャサイズ.Width) - this.ct上背景スクロール用タイマーD3[i].n現在の値, TJAPlayer3.Skin.Background_Scroll_Y[i]);
                                                TJAPlayer3.Tx.Background_UpDanC4[i].t2D描画(TJAPlayer3.app.Device,  +(l * TJAPlayer3.Tx.Background_UpDanC4[i].szテクスチャサイズ.Width) - this.ct上背景スクロール用タイマーDW[i].n現在の値, TJAPlayer3.Skin.Background_Scroll_Y[i] - 300 + this.ct上背景スクロール用タイマーD4[i].n現在の値);
                                                TJAPlayer3.Tx.Background_UpDanC3[i].t2D描画(TJAPlayer3.app.Device, +(l * TJAPlayer3.Tx.Background_UpDanC3[i].szテクスチャサイズ.Width) - this.ct上背景スクロール用タイマーD2[i].n現在の値, TJAPlayer3.Skin.Background_Scroll_Y[i]);

                                            }
                                        }

                                    }
                                }

            }
            #endregion
            #endregion
            #region 1P-下背景
            if ( !TJAPlayer3.stage演奏ドラム画面.bDoublePlay )
            {
                {
                    if( TJAPlayer3.Tx.Background_Down != null )
                    {
                        TJAPlayer3.Tx.Background_Down.t2D描画( TJAPlayer3.app.Device, 0, 360 );
                    }
                    if (TJAPlayer3.Tx.Background_Down2 != null)
                    {
                        TJAPlayer3.Tx.Background_Down2.Opacity = ((c * 0xff) / 100);
                        TJAPlayer3.Tx.Background_Down2.t2D描画(TJAPlayer3.app.Device, 0, 360);
                    }

                }
                if(TJAPlayer3.stage演奏ドラム画面.bIsAlreadyCleared[0] && TJAPlayer3.ConfigIni.eGaugeMode != EGaugeMode.Hard && TJAPlayer3.ConfigIni.eGaugeMode != EGaugeMode.ExHard )
                {
                    if( TJAPlayer3.Tx.Background_Down_Clear != null && TJAPlayer3.Tx.Background_Down_Scroll != null )
                    {
                        TJAPlayer3.Tx.Background_Down_Clear.Opacity = ( ( this.ct上背景FIFOタイマー.n現在の値 * 0xff ) / 100 );
                        TJAPlayer3.Tx.Background_Down_Scroll.Opacity = ( ( this.ct上背景FIFOタイマー.n現在の値 * 0xff ) / 100 );
                        TJAPlayer3.Tx.Background_Down_Scroll1.Opacity = ((this.ct上背景FIFOタイマー.n現在の値 * 0xff) / 100);
                        TJAPlayer3.Tx.Background_Down_Scroll2.Opacity = ((this.ct上背景FIFOタイマー.n現在の値 * 0xff) / 100);
                        TJAPlayer3.Tx.Background_Down_UD.Opacity = ((this.ct上背景FIFOタイマー.n現在の値 * 0xff) / 100);

                        //int nループ幅 = 1257;
                        //CDTXMania.Tx.Background_Down_Scroll.t2D描画( CDTXMania.app.Device, 0 - this.ct下背景スクロール用タイマー1.n現在の値, 360 );
                        //CDTXMania.Tx.Background_Down_Scroll.t2D描画(CDTXMania.app.Device, (1 * nループ幅) - this.ct下背景スクロール用タイマー1.n現在の値, 360);
                        double TexSize = 1280 / TJAPlayer3.Tx.Background_Down_Scroll.szテクスチャサイズ.Width;
                        // 1280をテクスチャサイズで割ったものを切り上げて、プラス+1足す。
                        int ForLoop = (int)Math.Ceiling(TexSize) + 1;

                        //int nループ幅 = 328;
                        TJAPlayer3.Tx.Background_Down_Scroll.t2D描画(TJAPlayer3.app.Device, 0 - this.ct下背景スクロール用タイマー1.n現在の値, 360);
                        for (int l = 1; l < ForLoop + 1; l++)
                        {
                            TJAPlayer3.Tx.Background_Down_Scroll.t2D描画(TJAPlayer3.app.Device, +(l * TJAPlayer3.Tx.Background_Down_Scroll.szテクスチャサイズ.Width) - this.ct下背景スクロール用タイマー1.n現在の値, 360);
                        }
                        TJAPlayer3.Tx.Background_Down_Clear.t2D描画(TJAPlayer3.app.Device, 0, 360);

                        TJAPlayer3.Tx.Background_Down_Scroll1.t2D描画(TJAPlayer3.app.Device, 0 - this.ct下背景スクロール用タイマー2.n現在の値, 360);
                        for (int l = 1; l < ForLoop + 1; l++)
                        {
                            TJAPlayer3.Tx.Background_Down_Scroll1.t2D描画(TJAPlayer3.app.Device, +(l * TJAPlayer3.Tx.Background_Down_Scroll1.szテクスチャサイズ.Width) - this.ct下背景スクロール用タイマー2.n現在の値, 360);
                        }
                        TJAPlayer3.Tx.Background_Down_Scroll2.t2D描画(TJAPlayer3.app.Device, 0 - this.ct下背景スクロール用タイマー3.n現在の値, 360);
                        for (int l = 1; l < ForLoop + 1; l++)
                        {
                            TJAPlayer3.Tx.Background_Down_Scroll2.t2D描画(TJAPlayer3.app.Device, +(l * TJAPlayer3.Tx.Background_Down_Scroll2.szテクスチャサイズ.Width) - this.ct下背景スクロール用タイマー3.n現在の値, 360);
                        }
                        if (this.ct下背景上下用 != null)
                        {
                            double 回転値 = 180.0;
                            double U = Math.Sin(this.ct下背景上下用.n現在の値 * Math.PI / (this.ct下背景上下用.n終了値 * 180.0 / 回転値)) * 35;

                            if (TJAPlayer3.Tx.Background_Down_UD != null)
                            {
                                TJAPlayer3.Tx.Background_Down_UD.t2D描画(TJAPlayer3.app.Device, 0, (float)U + 320);
                            }

                        }
                    }

                   
                }
            }
            
            #endregion
            return base.On進行描画();
        }

        #region[ private ]
        //-----------------
        private CCounter[] ct上背景スクロール用タイマー; //上背景のX方向スクロール用
        private CCounter[] ct上背景上スクロール用タイマー; //上背景のY方向スクロール用
        private CCounter[] ct上背景スクロール用タイマーD;
        private CCounter[] ct上背景スクロール用タイマーD1;
        private CCounter[] ct上背景スクロール用タイマーD2;
        private CCounter[] ct上背景スクロール用タイマーD3;
        private CCounter[] ct上背景スクロール用タイマーD4;
        private CCounter[] ct上背景スクロール用タイマーDW;
        private CCounter[] ct上背景スクロール用タイマーU;
        private CCounter[] ct上背景スクロール用タイマーU1;
        private CCounter[] ct上背景スクロール用タイマーUd;
        private CCounter[] ct上背景スクロール用タイマーUd1;
        private CCounter ct下背景スクロール用タイマー1;
        private CCounter ct下背景スクロール用タイマー2;//下背景パーツ1のX方向スクロール用
        private CCounter ct下背景スクロール用タイマー3;
        private CCounter ct下背景上下用;
        private CCounter cttoumeib;
        private CCounter ct上背景FIFOタイマー;
        private CCounter[] ct上背景クリアインタイマー;
        //private CTexture tx上背景メイン;
        //private CTexture tx上背景クリアメイン;
        //private CTexture tx下背景メイン;
        //private CTexture tx下背景クリアメイン;
        //private CTexture tx下背景クリアサブ1;
        //-----------------
        #endregion
    }
}
　
